<?php wp_nonce_field( "edit_redirs", "redir_box", 1, 1 ); ?>
<div class="onetshp-wrap side">
	<p class="row">
		<label><?php _e('On simple request','onetshp'); ?></label>
		<div class="opts">
			<input type="radio" name="oshp_redir_simple" value="0" <?php echo get_post_meta($post->ID,"redir_simple",1) != 1 ? ' checked="checked"' : ""; ?> /> <?php _e('Serve the article','onetshp'); ?>
			<input type="radio" name="oshp_redir_simple" value="1" <?php echo get_post_meta($post->ID,"redir_simple",1) ? ' checked="checked"' : ""; ?> /> <?php _e('Redirect to','onetshp'); ?>
			<input type="text" name="oshp_redir_simple_url" value="<?php echo get_post_meta($post->ID,"redir_simple_url",1); ?>" />
		</div>
	</p>
</div>