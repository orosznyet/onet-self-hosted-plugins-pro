<?php
/*
===================================================================
Plugin Name:  ONet Self-hosted Plugins Pro
Plugin URI:   http://onetdev.com/repo/onet-self-hosted-plugins-pro
Description:  Host your very own plugins with no third party services included while each plugins remain updatable. This plugin is ideal for paid plugins as well since you can set your own "update rules" for each plugin.
Git:          https://bitbucket.org/orosznyet/onet-self-hosted-plugins-pro
Version:      1.0
Author:       József Koller
Author URI:   http://www.onetdev.com
Text Domain:  onetshp
Domain Path:  /lang

===================================================================

Copyright (C) 2013 József Koller

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

===================================================================
*/


// Register init function
add_action( 'plugins_loaded', 'ONetSHP_wrap' );
function ONetSHP_wrap() {
global $ONetSHP_inst;
	$ONetSHP_inst = new ONetSHP();
}

// Installation stuff
register_activation_hook(__FILE__,'ONetSHP_install');
function ONetSHP_install () {
	
	# Check if PHP version is too low and throw error if yes.
	if ( version_compare("5.0", phpversion(), '>') ) {
		echo '<strong>'.__('Plugin installation failed.','onetshp').'</strong> '.
			sprintf(__('Sorry, ONet Self-hosted Plugins Pro requires at least PHP 5.0 but your version is PHP %s.','onetshp'),phpversion());
		exit;
	}

	# Otherwise execute little code
	else { }
}

// Then the class
class ONetSHP {
	protected $menu_item;                  // Menu item's hook
	protected $ptype;                      // Post admin data
	protected $opts = array();             // options
	protected $home_folder = null;         // Plugin home directory
	protected $home_url = null;            // Plugin home url
	protected $hidden_folder = null;       // Cache folder
	protected $post_type = null;           // The name of the custom post type used as repo holder
	protected $views = null;               // View folder contains the view files


	/*********************************
	* Constructing & Registering
	*********************************/



	/**
	* Register all important functions and filters
	* @since 1.0
	* @param void
	* @return void
	**/
	public function __construct() {
		# Set some default variable
		$this->plugin = dirname(plugin_basename( __FILE__ ));
		$this->home_folder = __DIR__;
		$this->home_url = plugins_url( '' , __FILE__ );
		$this->hidden_folder = $this->home_folder."/protected";
		$this->views = $this->home_folder."/view";

		
		// Load localizations. You can add you own localisation file (eg: onetshp-hu_HU.mo) into lang folder.
		load_plugin_textdomain('onetshp', false, $this->home_folder.'/lang');

		// Load options
		$default_opts = array(
			"repo_base" =>                 'repo' // Display button after "upload media" in post editor
			);
		$custom_opts = get_option("onetshp_opts", array());
		$this->opts = (object)array_merge($default_opts,$custom_opts);
		$this->post_type = 'onetshp_'.$this->opts->repo_base;

		# Register actions
		add_action('init',                  array(&$this,"on_initalize") );
		add_action('admin_menu',            array(&$this,"admin_menu") );
		add_action('wp',                    array(&$this,"capture_post") );
		add_action('add_meta_boxes',        array(&$this,"add_repo_metaboxes") );
		add_action('save_post',             array(&$this,"save_metaboxes"), 1, 2);

		# Apply filters
		// none
	}

	/**
	* Initalization script
	* Register custom post types
	* @since 1.0
	* @param void
	* @return null
	**/
	function on_initalize () {
	global $wp_rewrite;
		# Register repos post type
		$labels = array(
			'name'						=> __('Hosted Plugins','onetshp'),
			'singular_name'				=> __('Hosted Plugin','onetshp'),
			'add_new'					=> __('Add New','onetshp'),
			'add_new_item'				=> __('Add New Hosted Plugin','onetshp'),
			'edit_item'					=> __('Edit Hosted Plugins','onetshp'),
			'new_item'					=> __('New Item','onetshp'),
			'all_items'					=> __('All Items','onetshp'),
			'view_item'					=> __('View Item','onetshp'),
			'search_items'				=> __('Search Items','onetshp'),
			'not_found'					=> __('No items found','onetshp'),
			'not_found_in_trash'		=> __('No items found in the Trash','onetshp') ,
			'parent_item_colon'			=> '',
			'menu_name'					=> __('Hosted Plugins','onetshp')
		);

		$args = array(
			'labels'					=> $labels,
			'description'				=> __('Self-hosted plugins of this website.','onetshp'),
			'public'					=> true,
			'has_archive'				=> true,
			'exclude_from_search'		=> false,
			'publicly_queryable'		=> true,
			'menu_icon'					=> $this->home_url.'/asset/img/menu-icon.png',
			'supports'					=> array('title', 'editor', 'thumbnail', 'trackbacks', 'revisions'),
			'rewrite'					=> array('slug' => $this->opts->repo_base,'with_front' => false),
		);
		$this->ptype = register_post_type($this->post_type, $args);

	}

	/**
	* Add settings page for custom post type
	* @since 1.0
	* @param void
	* @return null
	**/
	function admin_menu () {
		# Add Settings page for 
		$this->menu_item = add_submenu_page('edit.php?post_type='.$this->post_type, __("Self-hosted Plugins Pro Settings","onetshp"), __("Settings","onetshp"), 'edit_posts', basename(__FILE__), array(&$this,"settings_controller"));
	}



	/*********************************
	* Wordpress "hacks"
	*********************************/



	/**
	* Capture and handle posts if they belongs to the plugin.
	* This party important for rerouting and updating queries from remote.
	* @since 1.0
	* @param (object) $wp object
	**/
	function capture_post ($data) {
	global $post;
		# Check if post type is correct and post is exsisting
		if (is_single() && $data->query_vars['post_type'] == $this->post_type && !empty($post)) {
			if (get_post_meta($post->ID,"redir_simple",1)) {
				header("Location: ".get_post_meta($post->ID,"redir_simple_url",1));
				exit;
			}
		}
	}



	/*********************************
	* SETTINGS
	*********************************/



	/**
	* Register settings page fors custom post type
	* @since 1.0
	* @param void
	* @return null
	**/
	function settings_controller () {
		echo 'Settings, yeah.';
	}



	/*********************************
	* REPO ADMIN
	*********************************/



	/**
	* Adds custom metaboxes for repo editor
	* @since 1.0
	* @param void
	* @return null
	**/
	function add_repo_metaboxes () {
		add_meta_box('onetshp_redirs', __('Traffic handling','onetshp'), array(&$this, "content_repo_metaboxes"), $this->post_type, 'side', 'default', 'redirs' );
	}

	/**
	* Draws content for metaboxes
	* @since 1.0
	* @param void
	* @return null
	**/
	function content_repo_metaboxes ($post,$data) {
	global $post;
		$vdata = new STDClass();
		$vdata->opts = $this->opts;

		$file = $this->views."/metabox.".$data['args'].".php";
		if (file_exists($file)) include $file;
		else echo __("Invalid metabox content.","onetshp");
	}

	/**
	* Save stuff
	**/
	function save_metaboxes($post_id, $post) {
		# Avoid arbitrary requests
		if ( !wp_verify_nonce( $_POST['redir_box'], 'edit_redirs' )) return $post->ID;
		if ( !current_user_can( 'edit_post', $post->ID )) return $post->ID;
		if ( $post->post_type != $this->post_type) return $post->ID;

		# Form items must be updated
		$items = array(
			(object)array(
				"name" => "redir_simple",
				"check" => "boolean"
				),
			(object)array(
				"name" => "redir_simple_url",
				"check" => "url"
				)
		);

		# Saving stuff
		foreach ($items AS $item) {
			$val = "";
			if (isset($_POST['oshp_'.$item->name])) $val = $_POST['oshp_'.$item->name];
			if ($item->check == "boolean") {
				if (!empty($val)) $val = true;
				else $val = 0;
			} else if ($item->check == "url") {
				$val = esc_url($val);
			}

			# Update me
			update_post_meta($post->ID, $item->name, $val);
		}
	}


	/*********************************
	* Utility methods
	*********************************/


}
?>